package raplex.lib.managers;

import org.bukkit.entity.Player;

import raplex.lib.utils.MessageUtils;
import raplex.lib.utils.MessageUtils.EnumTitleAction;

public class TitleManager {
	/**
	 * Sends a title packet with a {@link Player}
	 * @param player The {@link Player}
	 * @param action The title {@link EnumTitleAction}
	 * @param title The title message
	 * @param fadeIn The fadeIn time. Can be null if action isnt TIMES
	 * @param time The idle time. Can be null if action isnt TIMES
	 * @param fadeOut the fadeOut time. Can be null if action isnt TIMES
	 */
	public static void sendTitle(Player player, MessageUtils.EnumTitleAction action, String title, int fadeIn, int time, int fadeOut) {
		MessageUtils.sendTitle(player, action, title, fadeIn, time, fadeOut);
	}

	/**
	 * Sends a title with specific timings
	 * @param player The {@link Player}
	 * @param title The title message
	 * @param fadeIn The fadeIn time.
	 * @param time The idle time.
	 * @param fadeOut the fadeOut time.
	 */
	public static void sendTitle(Player player, String title, int fadeIn, int time, int fadeOut) {
		sendTitle(player, EnumTitleAction.TIMES, title, fadeIn, time, fadeOut);
		sendTitle(player, EnumTitleAction.TITLE, title, fadeIn, time, fadeOut);
	}

	/**
	 * Sends a subtitle with specific timings to a {@link Player}
	 * @param player The {@link Player}
	 * @param title The title message
	 * @param fadeIn The fadeIn time.
	 * @param time The idle time.
	 * @param fadeOut the fadeOut time.
	 */
	public static void sendSubTitle(Player player, String title, int fadeIn, int time, int fadeOut) {
		sendTitle(player, EnumTitleAction.TIMES, title, fadeIn, time, fadeOut);
		sendTitle(player, EnumTitleAction.SUBTITLE, title, fadeIn, time, fadeOut);
	}

	/**
	 * Sends a title with subtitle with specific timings to a {@link Player}
	 * @param player The {@link Player}
	 * @param title The title message
	 * @param subtitle The subtitle message
	 * @param fadeIn The fadeIn time.
	 * @param time The idle time.
	 * @param fadeOut the fadeOut time.
	 */
	public static void sendTitle(Player player, String title, String subtitle, int fadeIn, int time, int fadeOut) {
		sendTitle(player, EnumTitleAction.TIMES, title, fadeIn, time, fadeOut);
		sendTitle(player, title, fadeIn, time, fadeOut);
		sendSubTitle(player, subtitle, fadeIn, time, fadeOut);
	}

	/**
	 * Sends a title to a {@link Player} with default timings
	 * @param player The {@link Player}
	 * @param title The title message
	 */
	public static void sendTitle(Player player, String title) {
		sendTitle(player, title, -1, -1, -1);
	}

	/**
	 * Sends a subtitle to a {@link Player} with default timings
	 * @param player The {@link Player}
	 * @param subtitle The subtitle message
	 */
	public static void sendSubTitle(Player player, String subtitle) {
		sendSubTitle(player, subtitle, -1, -1, -1);
	}

	/**
	 * Sends a title with a subtitle to a {@link Player} with default timings
	 * @param player The {@link Player}
	 * @param title The title message
	 * @param subtitle The subtitle message
	 */
	public static void sendTitle(Player player, String title, String subtitle) {
		sendTitle(player, title, subtitle, -1, -1, -1);
	}
}
