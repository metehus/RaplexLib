package raplex.lib.managers;

import org.bukkit.entity.Player;

import raplex.lib.utils.MessageUtils;

public class MessageManager {
	/**
	 * Sends an ActionBar message to a Bukkit player
	 * @param player The Bukkit Player
	 * @param message The ActionBar message
	 */
	public static void sendActionBar(Player player, String message) {
		MessageUtils.sendActionBar(player, message);
	}
}
