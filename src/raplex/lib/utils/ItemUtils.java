package raplex.lib.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemUtils {
	public static ItemStack getItemFromStringId(String id) {
	    if (id.contains(":"))
	        return new ItemStack(Integer.parseInt(id.split(":")[0]), 1, Short.parseShort(id.split(":")[1]));
	    else
	        return new ItemStack(Integer.parseInt(id));
	}
}
