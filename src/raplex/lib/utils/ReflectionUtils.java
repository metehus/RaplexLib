package raplex.lib.utils;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ReflectionUtils {

	
	public static Class<?> getNMSClass(String name) {
		return ReflectionUtils.getClass("net.minecraft.server.{{version}}." + name);
	}
	
	public static Class<?> getOBClass(String name) {
		return ReflectionUtils.getClass("org.bukkit.craftbukkit.{{version}}." + name);
	}
	
	public static Class<?> getClass(String path) {
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		try {
			return Class.forName(path.replace("{{version}}", version));
		} catch (ClassNotFoundException | ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}
	
	public static void sendPacket(Player player, Object packet) {
		try {
			Object entityPlayer = player.getClass().getMethod("getHandle").invoke(player);
			Object playerConn = entityPlayer.getClass().getField("playerConnection").get(entityPlayer);
			playerConn.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConn, packet);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
}
