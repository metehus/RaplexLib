package raplex.lib.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class SerializationUtils {

	public static String serializeLocation(Location location) {
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();
		return String.join(";", location.getWorld().getName(), Integer.toString(x), Integer.toString(y), Integer.toString(z));
	}
	
	public static Location deserializeLocation(String location) {
		String[] a = location.split(";");
		return new Location(Bukkit.getWorld(a[0]), Double.parseDouble(a[1]), Double.parseDouble(a[2]), Double.parseDouble(a[3]));
	}
}
