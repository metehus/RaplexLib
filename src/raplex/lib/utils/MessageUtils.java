package raplex.lib.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R1.IChatBaseComponent;


public class MessageUtils {
	private static Class<?> icbc = ReflectionUtils.getNMSClass("IChatBaseComponent");


	/**
	 * Sends an action bar to a {@link Player}
	 * @param player The {@link Player}
	 * @param msg The actionbar message
	 */
	public static void sendActionBar(Player player, String msg) {
		try {
			Class<?> typeMessageClass;
			Object typeMessage;
			
			Class<?> ppoChat = ReflectionUtils.getNMSClass("PacketPlayOutChat");
		
			String v = Bukkit.getServer().getBukkitVersion().split("-")[0];
			
			
			if (Double.parseDouble(v.length() == 4 ? v : v.replace(".", ".0")) >= 1.12) {
				typeMessageClass = ReflectionUtils.getNMSClass("ChatMessageType");
				typeMessage = typeMessageClass.getEnumConstants()[2];
			} else {
				typeMessageClass = byte.class;
				typeMessage = (byte) 2;
			}
			
			Constructor<?> chatConstructor = ppoChat.getConstructor(icbc, typeMessageClass);
			
			Object packet = chatConstructor.newInstance(MessageUtils.getICBCMessage(msg), typeMessage);
			
			ReflectionUtils.sendPacket(player, packet);
		
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
			Bukkit.getLogger().severe("Failed to send action bar message to " + player.getName());
			e.printStackTrace();
		}
	}

	/**
	 * Sends an title to a {@link Player}
	 * @param player The {@link Player}
	 * @param action The title {@link EnumTitleAction}
	 * @param msg The title message
	 * @param fi The FadeIn value. Can be nullable if action isnt TIMES
	 * @param t The Times value. Can be nullable if action isnt TIMES
	 * @param fo The FadeOut value. Can be nullable if action isnt TIMES
	 */
	public static void sendTitle(Player player, EnumTitleAction action, String msg, int fi, int t, int fo) {
		try {
		
			Class<?> ppoChat = ReflectionUtils.getNMSClass("PacketPlayOutTitle");
			Class<?> titleActionEnum = ReflectionUtils.getNMSClass("EnumTitleAction");
			Object titleType = titleActionEnum.getEnumConstants()[action.getIndex()];
		
			
			Constructor<?> titleConstructor = ppoChat.getConstructor(titleActionEnum, icbc, int.class, int.class, int.class);
			
			Object packet = titleConstructor.newInstance(titleType, MessageUtils.getICBCMessage(msg), fi, t, fo);
			
			ReflectionUtils.sendPacket(player, packet);
		
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
			Bukkit.getLogger().severe("Failed to send action bar message to " + player.getName());
			e.printStackTrace();
		}
	}

	/**
	 * Creates a ChatSerializer from nms
	 * @param msg The message
	 * @return The ChatSerializer instance
	 */
	public static Object getICBCMessage(String msg) {
		try {
			Method a;
			if (icbc.getDeclaredClasses().length > 0) {
				a = icbc.getDeclaredClasses()[0].getMethod("a", String.class);
			} else {
				a = ReflectionUtils.getNMSClass("ChatSerializer").getMethod("a", String.class);
			}
			return a.invoke(null, "{\"text\":\"" + msg + "\"}");
			
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			Bukkit.getLogger().severe("Failed to get ICBC of message " + msg);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * The title action
	 */
	public enum EnumTitleAction {
		/**
		 * Sends a title
		 */
		TITLE(0),
		/**
		 * Sends a subtitle
		 */
		SUBTITLE(1),
		/**
		 * Sends a TIMES update, to update the fadeIn, faceOut and time from title
		 */
		TIMES(2),
		/**
		 * Clears the title
		 */
		CLEAR(3),
		/**
		 * Resets the title times
		 */
		RESET(4);

		
		private final int i;
		EnumTitleAction(int i) {
			this.i = i;
		}
		
		public int getIndex() {
			return i;
		}
	}
}