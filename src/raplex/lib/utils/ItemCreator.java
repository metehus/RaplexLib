package raplex.lib.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemCreator {
	private ItemStack item;
	private ItemMeta meta;

	public ItemCreator(Material mat) {
		item = new ItemStack(mat);
		meta = item.getItemMeta();
	}
	
	public ItemCreator(ItemStack item) {
		this.item = item;
		meta = item.getItemMeta();
	}
	
	public ItemCreator setAmount(int amount) {
		item.setAmount(amount);
		return this;
	}
	
	public ItemCreator setDurability(short durability) {
		item.setDurability(durability);
		return this;
	}
	
	public ItemCreator setDisplayName(String name, boolean translate) {
		if (name == null) return this;
		if (translate) name = ChatColor.translateAlternateColorCodes('&', name);
		meta.setDisplayName(name);
		return this;
	}
	
	public ItemCreator setDisplayName(String name) {
		return setDisplayName(name, false);
	}
	
	public ItemCreator setLore(List<String> lore, boolean format) {
		if (format) lore = ConfigUtils.formatList(lore);
		meta.setLore(lore);
		return this;
	}
	
	public ItemCreator setLore(List<String> lore) {
		return setLore(lore, false);
	}
	
	public ItemCreator addLore(String lore) {
		List<String> l = meta.getLore();
		l.add(lore);
		meta.setLore(l);
		return this;
	}
	
	public ItemCreator setHeadFromName(String name) {
		setHead();
		item = SkullCreator.itemWithName(item, name);
		return this;
	}
	
	public ItemCreator setHeadFromUUID(UUID uuid) {
		setHead();
		item = SkullCreator.itemWithUuid(item, uuid);
		return this;
	}
	
	public ItemCreator setHeadFromPlayer(OfflinePlayer player) {
		setHead();
		item = SkullCreator.itemWithUuid(item, player.getUniqueId());
		return this;
	}
	
	public ItemCreator setHeadFromBase64(String base64) {
		setHead();
		item = SkullCreator.itemWithBase64(item, base64);
		return this;
	}

	public ItemCreator setHeadFromURL(String url) {
		setHead();
		item = SkullCreator.itemWithUrl(item, url);
		return this;
	}
	
	public void setHead() {
		item.setType(Material.SKULL_ITEM);
		item.setDurability((short) 3);
	}
	
	public ItemCreator addEnchant(Enchantment ench, int level, boolean ignoreLevelRestriction) {
		meta.addEnchant(ench, level, ignoreLevelRestriction);
		return this;
	}
	
	public ItemCreator hideEnchants() {
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		return this;
	}
	
	public ItemCreator hideAtributes() {
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		return this;
	}
	
	public ItemCreator hidePotionEfeects() {
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		return this;
	}

	public ItemCreator hideAll() {
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_UNBREAKABLE);
		return this;
	}
	
	public ItemStack create() {
		item.setItemMeta(meta);
		return item;
	}
}
