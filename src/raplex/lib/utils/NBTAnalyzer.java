package raplex.lib.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.inventory.ItemStack;

/**
 * A library for analyzing an Bukkit ItemStack nbt tags
 * with nms using reflection
 *
 * @author Matheus D.
 */
public class NBTAnalyzer extends NBTUtilBase {

	/**
	 * Creates an NBTAnalyzer with an Bukkit ItemStack
	 * @param item
	 */
	public NBTAnalyzer(ItemStack item) {
		this.item = item;
		
		setNbtTagCompound();
	}


	/**
	 * Get a boolean value from the item's NBTTags
	 * @param key The boolean value key
	 * @return The NBTTag value
	 */
	public boolean getBoolean(String key) {
		try {
			Method method = NBTTagCompoundClass.getDeclaredMethod("getBoolean", String.class);
			return (boolean) method.invoke(NBTTagCompound, key);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return false;
	}


	/**
	 * Get a string value from the item's NBTTags
	 * @param key The string value key
	 * @return The NBTTag value
	 */
	public String getString(String key) {
		try {
			Method method = NBTTagCompoundClass.getDeclaredMethod("getString", String.class);
			return (String) method.invoke(NBTTagCompound, key);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get a integer value from the item's NBTTags
	 * @param key The integer value key
	 * @return The NBTTag value
	 */
	public int getInteger(String key) {
		try {
			Method method = NBTTagCompoundClass.getDeclaredMethod("getInt", String.class);
			return (int) method.invoke(NBTTagCompound, key);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
