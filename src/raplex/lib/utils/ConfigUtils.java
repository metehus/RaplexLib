package raplex.lib.utils;

import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

public class ConfigUtils {

	public static List<String> formatList(List<String> l) {
		List<String> list = new ArrayList<String>();
		l.forEach(s -> {
			list.add(ChatColor.translateAlternateColorCodes('&', s));
		});
		return list;
	}
	
	public static String advancedListSepartor(List<String> items, String separator, String lastSeparator) {
		if (items.size() == 1) return items.get(0);
		if (items.size() == 2) return String.join(lastSeparator, items.toArray(new String[items.size()]));
		String last = items.get(items.size() - 1);
		items.remove(items.size() - 1);
		return String.join(separator, items.toArray(new String[items.size()])) + lastSeparator + last;
	}
}
