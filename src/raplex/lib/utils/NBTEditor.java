package raplex.lib.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.inventory.ItemStack;

/**
 * A library for editing an Bukkit ItemStack nbt tags
 * with nms using reflection
 *
 * @author Matheus D.
 */
public class NBTEditor extends NBTUtilBase {

	/**
	 * Returns an NBTEditor for an Bukkit Item
	 * @param item The bukkit ItemStack
	 */
	public NBTEditor(ItemStack item) {
		super.item = item;
		
		setNbtTagCompound();
	}

	/**
	 * Set a boolean to the nbt tag
	 * @param key The nbt value key
	 * @param value The nbt boolean value
	 * @return This NBTEditor
	 */
	public NBTEditor setBoolean(String key, boolean value) {
		try {
			Method setBoolean = super.NBTTagCompoundClass.getDeclaredMethod("setBoolean", String.class, boolean.class);
			setBoolean.invoke(NBTTagCompound, key, value);
			
			return this;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return this;
	}


	/**
	 * Set a string to the nbt tag
	 * @param key The nbt value key
	 * @param value The nbt string value
	 * @return This NBTEditor
	 */
	public NBTEditor setString(String key, String value) {
		try {
			Method setString = NBTTagCompoundClass.getDeclaredMethod("setString", String.class, String.class);
			setString.invoke(NBTTagCompound, key, value);
			
			return this;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return this;
	}


	/**
	 * Sets an integer to the nbt tag
	 * @param key The nbt value key
	 * @param value The nbt string value
	 * @return This NBTEditor
	 */
	public NBTEditor setInteger(String key, int value) {
		try {
			Method setString = NBTTagCompoundClass.getDeclaredMethod("setInt", String.class, int.class);
			setString.invoke(NBTTagCompound, key, value);

			return this;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return this;
	}


	/**
	 * Creates the itemstack
	 * @return The itemstack with the NBTTags
	 */
	public ItemStack create() {
		Method setTag;
		try {
			Object CraftIS = getCraftItemStack();
			setTag = ItemStackClass.getDeclaredMethod("setTag", NBTTagCompoundClass);
			setTag.invoke(CraftIS, NBTTagCompound);
			Method asBukkitCopy = CraftItemStackClass.getDeclaredMethod("asBukkitCopy", ItemStackClass);
			
			return (ItemStack) asBukkitCopy.invoke(null, CraftIS);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}
}
