package raplex.lib.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.inventory.ItemStack;

public class NBTUtilBase {
	protected ItemStack item;
	
	protected Class<?> CraftItemStackClass;
	protected Class<?> ItemStackClass;
	protected Class<?> NBTTagCompoundClass;
	protected Method asNMSCopy;
	protected Object NBTTagCompound;
	
	
	public void setNbtTagCompound() {
		try {
			CraftItemStackClass = ReflectionUtils.getOBClass("inventory.CraftItemStack");
			ItemStackClass = ReflectionUtils.getNMSClass("ItemStack");
			NBTTagCompoundClass = ReflectionUtils.getNMSClass("NBTTagCompound");
			asNMSCopy = CraftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
			Method getNBTTag = ItemStackClass.getMethod("getTag");
			boolean hasNBTTag = (boolean) ItemStackClass.getMethod("hasTag").invoke(getCraftItemStack());
			
			if (hasNBTTag) {
				this.NBTTagCompound = getNBTTag.invoke(getCraftItemStack());
			} else {
				this.NBTTagCompound = NBTTagCompoundClass.newInstance();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Object getCraftItemStack() {
		try {
			Class<?> CraftItemStackClass = ReflectionUtils.getOBClass("inventory.CraftItemStack");
			Class<?> ItemStackClass = ReflectionUtils.getNMSClass("ItemStack");
			Method asNMSCopy;
			asNMSCopy = CraftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
			return asNMSCopy.invoke(null, item);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
}
