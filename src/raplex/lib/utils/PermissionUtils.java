package raplex.lib.utils;

import org.bukkit.entity.Player;

public class PermissionUtils {

	public static int getPlayerLimitOnPermission(Player player, String perm, int max) {
        int limit = max;
        boolean found = false;
        while (!found) {
            if (limit < 0) break;
            if (player.hasPermission(perm + limit)) found = true;
            else limit--;
        }
        return limit;
    }
}
